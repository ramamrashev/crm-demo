package com.innopolis.crm.model;

import lombok.*;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Builder(toBuilder = true)
public class Employee {
    private UUID id;
    private String surname;
    private String name;
    private String telephone;
}
