package com.innopolis.crm.model;

import lombok.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Builder(toBuilder = true)
public class Invoice {
    private UUID id;
    private LocalDate date;
    private UUID customerId;
    private Map<UUID, Integer> order;
    private UUID employeeID;
}
