package com.innopolis.crm.model;

import lombok.*;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Builder(toBuilder = true)
public class Product {
    private UUID id;
    private String name;
    private String email;
    private Double price;
}
