package com.innopolis.crm.model;

import lombok.*;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@Builder(toBuilder = true)
public class Application {
    private UUID id;
    private String status;
    private UUID customerId;
    private List<UUID> productId;
    private UUID employeeID;
}
