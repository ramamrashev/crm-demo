package com.innopolis.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrmDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrmDemoApplication.class, args);
    }

}
